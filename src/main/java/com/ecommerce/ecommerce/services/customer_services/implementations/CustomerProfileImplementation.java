package com.ecommerce.ecommerce.services.customer_services.implementations;

import com.ecommerce.ecommerce.models.entities.CustomerProfile;
import com.ecommerce.ecommerce.models.entities.User;
import com.ecommerce.ecommerce.models.repositories.CustomerProfileRepository;
import com.ecommerce.ecommerce.models.repositories.UserRepository;
import com.ecommerce.ecommerce.services.customer_services.interfaces.ICustomerProfileCrudService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerProfileImplementation implements ICustomerProfileCrudService {
    private final UserRepository userRepository;
    private final CustomerProfileRepository customerProfileRepository;

    @Autowired
    public CustomerProfileImplementation(UserRepository userRepository, CustomerProfileRepository customerProfileRepository) {
        this.userRepository = userRepository;
        this.customerProfileRepository = customerProfileRepository;
    }

    @Override
    public List<User> list() {
        return this.userRepository.findAll();
    }

    @Override
    public User retrieve(String id) {
        Optional<User> user = this.userRepository.findById(id);
        if(user.isEmpty())
        {
            throw new EntityNotFoundException("instance not found wih id = " + id);
        }
        return user.get();
    }

    @Override
    public CustomerProfile updateProfile(String id, CustomerProfile customerProfile) {
        CustomerProfile user = this.retrieve(id).getClient();
        user.setFirstname(customerProfile.getFirstname());
        user.setLastname(customerProfile.getLastname());
        user.setPhone(customerProfile.getPhone());
        return customerProfileRepository.save(user);
    }

    @Override
    public void delete(String id) {
        userRepository.delete(retrieve(id));
    }

    public User create(User user)
    {
        if(user.getClient() == null) {
            throw new EntityNotFoundException("USER MUST HAVE A PROFILE");
        }
        user = this.userRepository.save(user);
        user.setClient(this.customerProfileRepository.save(user.getClient()));
        return user;
    }
}
