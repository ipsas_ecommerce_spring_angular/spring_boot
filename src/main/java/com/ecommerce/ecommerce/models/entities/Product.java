package com.ecommerce.ecommerce.models.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity(name = "products")
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class Product {
    @Id @GeneratedValue @Getter private Integer id;
    @Column(name = "title") @NonNull @Getter @Setter private String title;
    @Column(name = "code") @NonNull @Getter @Setter
    private String code;
    @Column(name = "description") @NonNull @Getter @Setter private String description;
    @Column(name = "slug") @NonNull @Getter @Setter private String slug;
    @Column(name = "image") @NonNull @Getter @Setter private String image;
    @Column(name = "price") @NonNull @Getter @Setter Float price;
    @Column(name = "current_quantity") @NonNull @Getter @Setter Integer currentQuantity;
    @ManyToOne() @NonNull @Getter @Setter private Category category;
    @OneToMany(fetch = FetchType.LAZY) @Getter @Setter private List<Olfaction> olfactions;
    @Column(name = "number_purchases") @Getter @Setter private Integer numberPurchases;
}
